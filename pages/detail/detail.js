// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 存储视频详情数据
    videoDetail:{

    },
    //存储推荐视频列表
    othersList:[],
    //存储评论数据
    commentData:{}
  },

  // 根据url传过来的参数来设置视频详情页面数据
  getVideoDetail(data){
    let that=this;
    that.setData({
      videoDetail:data
    })
  },
  //根据id来获取推荐视频列表
  getOthersList(videoId){
    let that=this;
    wx.request({
      url:"https://www.fastmock.site/mock/ff9a0b479c4e7a3d0b3e92c089b74520/wxbilibili/othersList?id="+videoId,
      success:res=>{
        if(res.data.code===0){
          console.log("ok");
          that.setData({
            othersList:res.data.othersList
          })
        }
      },
      fail:res=>{
        console.log("fail");
      }
    })
  },
  //获取评论数据
  getCommentList(videoId){
    let that=this;
    wx.request({
      url:"https://www.fastmock.site/mock/ff9a0b479c4e7a3d0b3e92c089b74520/wxbilibili/commentsList?id="+videoId,
      success:res=>{
        // console.log(res);
        if(res.data.code===0){
          that.setData({
            commentData:res.data.commentData
          })
        }
      },
      fail:res=>{
        console.log(res);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    this.getVideoDetail(options);
    // console.log(this.data.videoDetail);
    this.getOthersList(options.id);
    this.getCommentList(options.id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})