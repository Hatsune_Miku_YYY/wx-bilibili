Page({

  /**
   * 页面的初始数据
   */
  data: {
    //当前导航索引
    currentIndexNav:0,
    //存放导航数据
    navList:[],
    //存放轮播图数据
    swiperList:[],
    // 存放视频列表数据
    videoList:[]
  },
  //导航点击
  navTap(e){
    //  console.log(e);
    this.setData({
     currentIndexNav:e.target.dataset.index
    })
  },
  //获取导航数据
  getNavList(){
    let that=this;
    wx.request({
      url: 'https://www.fastmock.site/mock/ff9a0b479c4e7a3d0b3e92c089b74520/wxbilibili/navList',
      success: res=>{
        if(res.data.code===0){
          that.setData({
            navList:res.data.data.navList
          })
        }
      },
      fail: res=>{
        console.log(res);
      }
    })
  },
  //获取轮播图数据
  getSwiperList(){
    let that=this;
    wx.request({
      url:'https://www.fastmock.site/mock/ff9a0b479c4e7a3d0b3e92c089b74520/wxbilibili/Swiper',
      success: res=>{
        // console.log(res);
        if(res.data.code===0){
         that.setData({
           swiperList:res.data.data.swiperList
         })
        }
      },
      fail:res=>{
        console.log(res);
      }
    })
  },
  // 获取视频列表数据
  getVideoList(){
    let that=this;
    wx.request({
      url:'https://www.fastmock.site/mock/ff9a0b479c4e7a3d0b3e92c089b74520/wxbilibili/videoList',
      success:res=>{
        if(res.statusCode==200){
          //  console.log(res);
          that.setData({
            videoList:res.data
          })
        }
      },
      fail: res=>{
        console.log(res);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取导航栏数据
    this.getNavList();
    //获取轮播图数据
    this.getSwiperList();
    //获取视频列表数据
    this.getVideoList();
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})